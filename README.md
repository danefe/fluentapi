# FluentApi - Overview

This repository is the home of inumerous implementations of fluent interface with the goal of making the readability of the source code close to that of ordinary written prose, essentially creating a domain-specific language within the interface.

# Content Repositories
* [ASP.NET Core Web Api](https://bitbucket.org/danefe/fluentapi/src/master/src/FluentApi.WebApi/)

### Notes

- For more information about Fluent Interfaces click [here](https://en.wikipedia.org/wiki/Fluent_interface) 