﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace FluentApi.Jwt {
    public static class ServiceCollectionExtensions {
        public static FluentApi.Jwt.IJwt AddJwt(this IServiceCollection services) {
            return new FluentApi.Jwt.Jwt(services);
        }

    }
}
