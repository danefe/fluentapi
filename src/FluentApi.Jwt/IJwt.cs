﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FluentApi.Jwt {
    public interface IJwt {
        // Context
        IJwt WithIssuer(string issuer);
        IJwt WithAudience(string audience);
        IJwt WithKey(string key);

        // Exit
        void Apply();
    }
}
